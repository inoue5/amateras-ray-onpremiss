# AMATERAS RAY ONPREMISS

## テスト及びオンプレ環境

1. 作業用ディレクトリを作成する(名称はなんでも構いません）
    ```
    mkdir amateras_lataset
    ```
   
1. gitコマンドでデータを取得します。（{token}の箇所でワンタイムトークンを利用してください）
    ```
    git init
    git remote add origin https://oauth2:{token}@gitlab.com/inoue5/amateras.git
   git fetch origin
   git branch develop/2.2.0.1/dev origin/develop/2.2.0.1/dev
   git checkout --force develop/2.2.0.1/dev
    ```
   
1. 作成されたディレクトリに移動します
    ```
    cd amateras
    ```
   
1. setupファイルを実行します
    ```
    bash setup.sh
    ```

1. `application_name`を入力（testと入力してください）
    ```
    application_name:xxx
    ```

1. `modeling_name`を入力（testmodと入力してください）
    ```
    modeling_name:xxx
    ```

1. `database_name`を入力（testと入力してください）
    ```
    database_name:xxx
    ```

1. テスト環境かどうかを指定。 `y`を押下。
    ```
    is test? (y/N): n
    ```

1. `frontend`ディレクトリに移動
    ```
    cd amateras/frontend
    ```

1. フロントエンドセットアップファイルを起動（初回は時間がかかります）
    ```
    bash setup.sh
    ```

1. 以下のような表示が出たら、 `ctrl + c`でプロセスを終了する
    ```
    Child html-webpack-plugin for "index.html":
         1 asset
        Entrypoint undefined = index.html
    ```

1. 上のディレクトリに戻る
    ```
    cd ..
    ```

1. コンテナを起動(初回は時間がかかります)
    ```
    docker-compose up application nginx db redis db_center
    ```

1. 進行が止まったらプロセスを抜ける
    ```
    ctrl + z
    ```
   
1. dbにテーブルを作成する
    ```
    docker-compose exec application flask db upgrade
    ```

1. `db_center`に接続
    ```
    docker-compose exec db_center /bin/bash
    ```

1. centerテーブルを作成
    ```
    mysql -h db_center -u center -p < setup.sql
    ```

1. パスワードを入力 -> `aiforce` と入力してください
    ```
    Enter password:
    ```
   
1. `exit` で接続を解除してください
    ```
    exit
    ```

1. customerを作成する
    ```
    docker-compose exec application flask init-customer
    ```

1. `project_cap` を設定する。 `10` と入力して、 `Enter` を押下してください。
    ```
    project_cap: 10
    ```

1. customerを作成すると、以下のような表示が出るので、`modelig id` をメモしておく
    ```
    created cusotmers!!!
    customer id xxxxxxxxxxxxx
    application id xxxxxxxxxxxxxxxx
    modeling id xxxxxxxxxxxxxxxxxx
    ```

1. .deveolpment/config.ini ファイルの末尾に 手順 17 でメモした `modeling id` を追記する
    ```
    pub_name = {modeling id}
    ```

1. ユーザー作成コマンド実行
    ```
    docker-compose exec application flask create-user
    ```

1. 以下のように表示されたら `n`を押下
    ```
    have user_id? [n] n
    ```

1. `Email:` `Name:` `Password:` `Confirm password:` を入力

1. 以下のように表示されたら `y`を押下。ユーザーが作成される。
    ```
    Create this user email:xxxx@aiforce.solutions  name:xxxx [n]: y
    ```

1. flowjsファイルをコピーします
    ```
    cp .development/override_js/flow.js node_modules/@flowjs/flow.js/dist/flow.js
    ```
   
1. npmで再度ビルドしてください
    ```
    npm start
    ```

1. 以下のような表示が出たら、 `ctrl + c`でプロセスを終了する
    ```
    Child html-webpack-plugin for "index.html":
         1 asset
        Entrypoint undefined = index.html
    ```

1. モデリングコンテナを立ち上げる
    ```
    docker-compose up modeling3
    ```

1. これで作業は完了です。 `localhost:8080`にアクセスして、機能を確認してください。
